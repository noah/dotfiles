{ ... }: {

  imports = [
    ./audio.nix
    ./boot.nix
    ./keyboard.nix
    ./monitors.nix
    ./mouse.nix
    ./networking.nix
    ./sleep.nix
    ./wifi.nix
  ];

}
