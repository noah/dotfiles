{ config, pkgs, lib, ... }: {

  config = lib.mkIf config.services.xserver.enable {

    gui.toggleBarCommand = "polybar-msg cmd toggle";

    home-manager.users.${config.user} = {

      services.polybar = {
        enable = true;
        package = pkgs.polybar.override {
          i3GapsSupport = true;
          pulseSupport = true;
          githubSupport = true;
        };
        script = "polybar &";
        config = {
          "bar/main" = {
            bottom = false;
            width = "100%";
            height = "22pt";
            radius = 0;
            # offset-y = -5;
            # offset-y = "5%";
            # dpi = 96;
            background = config.colorscheme.base01;
            foreground = config.colorscheme.base05;
            line-size = "3pt";
            border-top-size = 0;
            border-right-size = 0;
            border-left-size = 0;
            border-bottom-size = "4pt";
            border-color = config.colorscheme.base00;
            padding-left = 2;
            padding-right = 2;
            module-margin = 1;
            modules-left = "i3";
            modules-center = "xwindow";
            modules-right = "pulseaudio date";
            cursor-click = "pointer";
            cursor-scroll = "ns-resize";
            enable-ipc = true;
            tray-position = "right";
            # wm-restack = "generic";
            # wm-restack = "bspwm";
            # wm-restack = "i3";
            # override-redirect = true;
          };
          "module/i3" = let padding = 2;
          in {
            type = "internal/i3";
            pin-workspaces = false;
            show-urgent = true;
            strip-wsnumbers = true;
            index-sort = true;
            enable-click = true;
            wrapping-scroll = true;
            fuzzy-match = true;
            format = "<label-state> <label-mode>";
            label-focused = "%name%";
            label-focused-foreground = config.colorscheme.base01;
            label-focused-background = config.colorscheme.base05;
            label-focused-underline = config.colorscheme.base03;
            label-focused-padding = padding;
            label-unfocused = "%name%";
            label-unfocused-padding = padding;
            label-visible = "%name%";
            label-visible-underline = config.colorscheme.base01;
            label-visible-padding = padding;
            label-urgent = "%name%";
            label-urgent-foreground = config.colorscheme.base00;
            label-urgent-background = config.colorscheme.base08;
            label-urgent-underline = config.colorscheme.base0F;
            label-urgent-padding = padding;
          };
          "module/xworkspaces" = {
            type = "internal/xworkspaces";
            label-active = "%name%";
            label-active-background = config.colorscheme.base05;
            label-active-foreground = config.colorscheme.base01;
            label-active-underline = config.colorscheme.base03;
            label-active-padding = 1;
            label-occupied = "%name%";
            label-occupied-padding = 1;
            label-urgent = "%name%";
            label-urgent-background = config.colorscheme.base08;
            label-urgent-padding = 1;
            label-empty = "%name%";
            label-empty-foreground = config.colorscheme.base06;
            label-empty-padding = 1;
          };
          "module/xwindow" = {
            type = "internal/xwindow";
            label = "%title:0:60:...%";
          };
          # "module/filesystem" = {
          # type = "internal/fs";
          # interval = 25;
          # mount-0 = "/";
          # label-mounted = "%{F#F0C674}%mountpoint%%{F-} %percentage_used%%";
          # label-unmounted = "%mountpoint% not mounted";
          # label-unmounted-foreground = colors.disabled;
          # };
          "module/pulseaudio" = {
            type = "internal/pulseaudio";
            # format-volume-prefix = "VOL ";
            # format-volume-prefix-foreground = colors.primary;
            format-volume = "<ramp-volume> <label-volume>";
            # format-volume-background = colors.background;
            # label-volume-background = colors.background;
            format-volume-foreground = config.colorscheme.base0B;
            label-volume = "%percentage%%";
            label-muted = "ﱝ ---";
            label-muted-foreground = config.colorscheme.base03;
            ramp-volume-0 = "";
            ramp-volume-1 = "墳";
            ramp-volume-2 = "";
          };
          # "module/xkeyboard" = {
          # type = "internal/xkeyboard";
          # blacklist-0 = "num lock";
          # label-layout = "%layout%";
          # label-layout-foreground = colors.primary;
          # label-indicator-padding = 2;
          # label-indicator-margin = 1;
          # label-indicator-foreground = colors.background;
          # label-indicator-background = colors.secondary;
          # };
          # "module/memory" = {
          # type = "internal/memory";
          # interval = 2;
          # format-prefix = "RAM ";
          # format-prefix-foreground = colors.primary;
          # label = "%percentage_used:2%%";
          # };
          # "module/cpu" = {
          # type = "internal/cpu";
          # interval = 2;
          # format-prefix = "CPU ";
          # format-prefix-foreground = colors.primary;
          # label = "%percentage:2%%";
          # };
          # "network-base" = {
          # type = "internal/network";
          # interval = 5;
          # format-connected = "<label-connected>";
          # format-disconnected = "<label-disconnected>";
          # label-disconnected = "%{F#F0C674}%ifname%%{F#707880} disconnected";
          # };
          # "module/wlan" = {
          # "inherit" = "network-base";
          # interface-type = "wireless";
          # label-connected = "%{F#F0C674}%ifname%%{F-} %essid% %local_ip%";
          # };
          # "module/eth" = {
          # "inherit" = "network-base";
          # interface-type = "wired";
          # label-connected = "%{F#F0C674}%ifname%%{F-} %local_ip%";
          # };
          "module/date" = {
            type = "internal/date";
            interval = 1;
            date = "%d %b  %l:%M %p";
            date-alt = "%Y-%m-%d %H:%M:%S";
            label = "%date%";
            label-foreground = config.colorscheme.base0A;
            # format-background = colors.background;
          };
          "settings" = {
            screenchange-reload = true;
            pseudo-transparency = false;
          };
        };
      };

      xsession.windowManager.i3.config.startup = [{
        command = "systemctl --user restart polybar";
        always = true;
        notification = false;
      }];

    };
  };

}
