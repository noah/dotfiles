{ ... }: {

  imports = [
    ./1password.nix
    ./alacritty.nix
    ./calibre.nix
    ./discord.nix
    ./firefox.nix
    ./media.nix
    ./obsidian.nix
    ./qbittorrent.nix
    ./nautilus.nix
  ];

}
